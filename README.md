Getting Started
---------------

To get started with PAC-man for shooter, you'll need to get
familiar with [Git and Repo](http://source.android.com/download/using-repo).

To initialize your local repository using the PAC-man trees, use a command like this:

    repo init -u git@bitbucket.org:cmartinbaughman/android.git -b jellybean

Then to sync up:

    repo sync